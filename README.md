# Squoosh 中文版

Squoosh 是Google推出的一款图像压缩工具，优点是本地化压缩、支持批量压缩，甚至还有桌面版的APP。

#### API & CLI

Squoosh has [an API](https://github.com/GoogleChromeLabs/squoosh/tree/dev/libsquoosh) and [a CLI](https://github.com/GoogleChromeLabs/squoosh/tree/dev/cli) to compress many images at once.

#### 隐私

Squoosh不会把你的图片发送到服务器，所有图像压缩都在本地处理。
不过，Squoosh会利用谷歌Analytics收集以下信息:

- [Basic visitor data](https://support.google.com/analytics/answer/6004245?ref_topic=2919631).
- 图片修改前与修改后的尺寸；
- 如果是Squoosh PWA，则记录Squoosh安装的类型；
- 如果是Squoosh PWA，则记录Squoosh安装时间和日期；

#### 使用

1. 安装
   ```sh
   npm install
   ```
2. 打包
   ```sh
   npm run build
   ```
3. 本地运行
   ```sh
   npm run dev
   ```
#### 贡献

Squoosh是一个非常感谢所有社区参与的开源项目。要对项目做出贡献，请遵循[贡献指南](/CONTRIBUTING.md).
